
function ValidateEmail()
	{
		var email = document.getElementById("EmailInputText");
		var mailformat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		var result = document.getElementById("result");
		
		var checksenderprenameinput = document.getElementById("PreNameInputText").value; 
		var checksendersurnameinput = document.getElementById("SurNameInputText").value;
		var checksenderemailinput = document.getElementById("EmailInputText").value;
		
		if(checksenderprenameinput.length == 0 || checksendersurnameinput.length == 0 || checksenderemailinput.length == 0){
			result.innerHTML = "Bitte füllen Sie alle Felder aus.";
			result.style.color = "red";
			return false;
		}
		
		if(email.value.match(mailformat)){
			result.innerHTML = email.value + "\n ist eine valide E-Mail-Adresse.";
			result.style.color = "green";
			
			//Open-Standard-Email-Program
			sendEmail();

			// Open-Code-Modal
			//modal();
			
			return true;
		}
		
		else{ 
			result.innerHTML = email.value + "\n ist keine valide E-Mail-Adresse.";
			result.style.color = "red";
			return false;
		}
	}

function sendEmail(){
		var emailTo = "info^bz-kassel.de";
		var emailCC = "kolearn^wi-kassel.de";
		var emailSub = "Anfrage für Informationen zu kostenlosen Vernetzungsaktivitäten oder Info-Veranstaltungen"; 
		var senderemail = document.getElementById("EmailInputText").value;
		var sendername = document.getElementById("PreNameInputText").value + ' ' + document.getElementById("SurNameInputText").value;
	    var emailBody = "Sehr geehrte Damen und Herren des Projekts KoLeArn,\n\nbitte schicken Sie mir gerne Informationen zu kostenlosen Vernetzungsaktivitäten oder Info-Veranstaltungen an die folgende E-Mail-Adresse:\n\n" + senderemail + "\n\nVielen Dank für Ihre Bemühungen.\n\nHerzliche Grüße\n\n" + sendername;
	    window.open(String('mailto:'+emailTo+'?cc='+emailCC+'&subject='+encodeURIComponent(emailSub)+'&body='+encodeURIComponent(emailBody)).replaceAll('^','@'),'_self');
	}
	
	
// https://www.w3schools.com/howto/howto_css_modals.asp

/*

function modal(){

// Get the modal
var modal = document.getElementById("CodeModal");
modal.style.display = "block";

// Get the button that opens the modal
//var btn = document.getElementById("buttonemaildownload");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}



}
*/