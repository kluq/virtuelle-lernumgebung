/*
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

(function() {
  var Marzipano = window.Marzipano;
  var bowser = window.bowser;
  var screenfull = window.screenfull;
  var data = window.data;

  // Grab elements from DOM.
  var panoElement = document.querySelector('#pano');
  var sceneNameElement = document.querySelector('#titleBar .sceneName');
  var sceneListElement = document.querySelector('#sceneList');
  var sceneElements = document.querySelectorAll('#sceneList .scene');
  var sceneListToggleElement = document.querySelector('#sceneListToggle');
  var autorotateToggleElement = document.querySelector('#autorotateToggle');
  var fullscreenToggleElement = document.querySelector('#fullscreenToggle');

  // Detect desktop or mobile mode.
  if (window.matchMedia) {
    var setMode = function() {
      if (mql.matches) {
        document.body.classList.remove('desktop');
        document.body.classList.add('mobile');
      } else {
        document.body.classList.remove('mobile');
        document.body.classList.add('desktop');
      }
    };
    var mql = matchMedia("(max-width: 500px), (max-height: 500px)");
    setMode();
    mql.addListener(setMode);
  } else {
    document.body.classList.add('desktop');
  }

  // Detect whether we are on a touch device.
  document.body.classList.add('no-touch');
  window.addEventListener('touchstart', function() {
    document.body.classList.remove('no-touch');
    document.body.classList.add('touch');
  });

  // Use tooltip fallback mode on IE < 11.
  if (bowser.msie && parseFloat(bowser.version) < 11) {
    document.body.classList.add('tooltip-fallback');
  }

  // Viewer options.
  var viewerOpts = {
    controls: {
      mouseViewMode: data.settings.mouseViewMode
    }
  };

  // Initialize viewer.
  var viewer = new Marzipano.Viewer(panoElement, viewerOpts);

  // Create scenes.
  var scenes = data.scenes.map(function(data) {
    var urlPrefix = "img/360-degree-images";
    var source = Marzipano.ImageUrlSource.fromString(
      urlPrefix + "/" + data.id + "/" + "left" + "/{z}/{f}/{y}/{x}.jpg",
      { cubeMapPreviewUrl: urlPrefix + "/" + data.id + "/" + "left" + "/preview.jpg" });
    var geometry = new Marzipano.CubeGeometry(data.levels);

    var limiter = Marzipano.RectilinearView.limit.traditional(data.faceSize, 100*Math.PI/180, 120*Math.PI/180);
    var view = new Marzipano.RectilinearView(data.initialViewParameters, limiter);

    var scene = viewer.createScene({
      source: source,
      geometry: geometry,
      view: view,
      pinFirstLevel: true
    });

    // Create link hotspots.
    /*data.linkHotspots.forEach(function(hotspot) {
      var element = createLinkHotspotElement(hotspot);
      scene.hotspotContainer().createHotspot(element, { yaw: hotspot.yaw, pitch: hotspot.pitch });
    });*/

    // Create info hotspots.
    data.infoHotspots.forEach(function(hotspot) {

      var container = scene.hotspotContainer();

      if(hotspot.style === "modal"){
        var element = createModalHotspotElement(hotspot);
      }

      else if (hotspot.style === "iframespot")
      {
        var element =  createEmbeddediframespotHotspotElement(hotspot);
      }

      else if (hotspot.style === "iframeselect")
      {
        var element =  createEmbeddediframeselectHotspotElement(hotspot);
      }

      else if (hotspot.style === "onlytext"){
        var element = createOnlyTextHotspotElement(hotspot);
      }

      else if(hotspot.style === "expand") {
        var element = createExpandHotspotElement(hotspot);
      }

      else if(hotspot.style === "hintspot") {
        var element = createHintspotHotspotElement(hotspot);
      }

      else if(hotspot.style === "infocustom") {
        var element = createInfoCustomHotspotElement(hotspot);
      }

      else if(hotspot.style === "reveal") {
        var element = createRevealHotspotElement(hotspot);
      }

      else if(hotspot.style === "rotate") {
        var element = createRotateHotspotElement(hotspot);
      }

      else if(hotspot.style === "textInfo") {
        var element = createtextInfoHotspotElement(hotspot);
      }

      else if(hotspot.style === "tooltip") {
        var element = createtooltipHotspotElement(hotspot);
      }

      else{
        var element = createInfoHotspotElement(hotspot);
      }

      container.createHotspot(element, { yaw: hotspot.yaw, pitch: hotspot.pitch },
          { perspective: { radius: hotspot.radius, extraTransforms: hotspot.extraTransforms }});

    });

    return {
      data: data,
      scene: scene,
      view: view
    };
  });

  // Set up autorotate, if enabled.
  var autorotate = Marzipano.autorotate({
    yawSpeed: 0.03,
    targetPitch: 0,
    targetFov: Math.PI/2
  });
  if (data.settings.autorotateEnabled) {
    autorotateToggleElement.classList.add('enabled');
  }

  // Set handler for autorotate toggle.
  autorotateToggleElement.addEventListener('click', toggleAutorotate);

  // Set up fullscreen mode, if supported.
  if (screenfull.enabled && data.settings.fullscreenButton) {
    document.body.classList.add('fullscreen-enabled');
    fullscreenToggleElement.addEventListener('click', function() {
      screenfull.toggle();
    });
    screenfull.on('change', function() {
      if (screenfull.isFullscreen) {
        fullscreenToggleElement.classList.add('enabled');
      } else {
        fullscreenToggleElement.classList.remove('enabled');
      }
    });
  } else {
    document.body.classList.add('fullscreen-disabled');
  }

  // Set handler for scene list toggle.
  sceneListToggleElement.addEventListener('click', toggleSceneList);

  // Start with the scene list open on desktop.
  if (!document.body.classList.contains('mobile')) {
    showSceneList();
  }

  // Set handler for scene switch.
  scenes.forEach(function(scene) {
    var el = document.querySelector('#sceneList .scene[data-id="' + scene.data.id + '"]');
    el.addEventListener('click', function() {
      switchScene(scene);
      // On mobile, hide scene list after selecting a scene.
      if (document.body.classList.contains('mobile')) {
        hideSceneList();
      }
    });
  });

  // DOM elements for view controls.
  var viewUpElement = document.querySelector('#viewUp');
  var viewDownElement = document.querySelector('#viewDown');
  var viewLeftElement = document.querySelector('#viewLeft');
  var viewRightElement = document.querySelector('#viewRight');
  var viewInElement = document.querySelector('#viewIn');
  var viewOutElement = document.querySelector('#viewOut');

  // Dynamic parameters for controls.
  var velocity = 0.7;
  var friction = 3;

  // Associate view controls with elements.
  var controls = viewer.controls();
  controls.registerMethod('upElement',    new Marzipano.ElementPressControlMethod(viewUpElement,     'y', -velocity, friction), true);
  controls.registerMethod('downElement',  new Marzipano.ElementPressControlMethod(viewDownElement,   'y',  velocity, friction), true);
  controls.registerMethod('leftElement',  new Marzipano.ElementPressControlMethod(viewLeftElement,   'x', -velocity, friction), true);
  controls.registerMethod('rightElement', new Marzipano.ElementPressControlMethod(viewRightElement,  'x',  velocity, friction), true);
  controls.registerMethod('inElement',    new Marzipano.ElementPressControlMethod(viewInElement,  'zoom', -velocity, friction), true);
  controls.registerMethod('outElement',   new Marzipano.ElementPressControlMethod(viewOutElement, 'zoom',  velocity, friction), true);

  function sanitize(s) {
    return s.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;');
  }

  function switchScene(scene) {
    stopAutorotate();
    scene.view.setParameters(scene.data.initialViewParameters);
    scene.scene.switchTo();
    startAutorotate();
    updateSceneName(scene);
    updateSceneList(scene);
  }

  function updateSceneName(scene) {
    sceneNameElement.innerHTML = sanitize(scene.data.name);
  }

  function updateSceneList(scene) {
    for (var i = 0; i < sceneElements.length; i++) {
      var el = sceneElements[i];
      if (el.getAttribute('data-id') === scene.data.id) {
        el.classList.add('current');
      } else {
        el.classList.remove('current');
      }
    }
  }

  function showSceneList() {
    sceneListElement.classList.add('enabled');
    sceneListToggleElement.classList.add('enabled');
  }

  function hideSceneList() {
    sceneListElement.classList.remove('enabled');
    sceneListToggleElement.classList.remove('enabled');
  }

  function toggleSceneList() {
    sceneListElement.classList.toggle('enabled');
    sceneListToggleElement.classList.toggle('enabled');
  }

  function startAutorotate() {
    if (!autorotateToggleElement.classList.contains('enabled')) {
      return;
    }
    viewer.startMovement(autorotate);
    viewer.setIdleMovement(3000, autorotate);
  }

  function stopAutorotate() {
    viewer.stopMovement();
    viewer.setIdleMovement(Infinity);
  }

  function toggleAutorotate() {
    if (autorotateToggleElement.classList.contains('enabled')) {
      autorotateToggleElement.classList.remove('enabled');
      stopAutorotate();
    } else {
      autorotateToggleElement.classList.add('enabled');
      startAutorotate();
    }
  }

  /* ----------------------------------------------------- */
  /* --- Embedded-HOTSPOT --- */

  function createEmbeddediframespotHotspotElement(hotspot) {

    var div = document.createElement('div');
    div.id = "iframespot";

    var div2 = document.createElement('div');
    div2.classList.add('message');
    div2.innerHTML = "Wählen Sie die Wiedergabe aus!";

    div.appendChild(div2);

    return div;
  }

  function createEmbeddediframeselectHotspotElement(hotspot) {

    var ul = document.createElement('ul');
    ul.id = "iframeselect";

    var li = document.createElement('li');
    li.setAttribute('data-source','googleMaps');
    li.innerHTML = "Veranstaltungsort";

    var li2 = document.createElement('li');
    li2.setAttribute('data-source','youtube');
    li2.innerHTML = "KoLeArn - Video";

    ul.appendChild(li);
    ul.appendChild(li2);

    return ul;
  }

  /*
  // Get the hotspot container for scene.
  var container = scene.hotspotContainer();
  // Create hotspot with different sources.
  container.createHotspot(document.getElementById('iframespot'), {yaw: 0.0335, pitch: -0.102},
      {perspective: {radius: 1640, extraTransforms: "rotateX(5deg)"}});
  container.createHotspot(document.getElementById('iframeselect'), {yaw: -0.35, pitch: -0.239});
  */

  // HTML sources.
  var hotspotHtml = {
    youtube: '<iframe id="youtube" width="1280" height="480" src="material/videos/How-to-KluQ-Lernplattform.mp4" frameborder="0" allowfullscreen></iframe>',
    googleMaps: '<iframe id="googlemaps" width="1280" height="480" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7058.425397513943!2d9.495022967519311!3d51.28296575447786!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47bb474f6724561f%3A0xb777bf1ae2234114!2sBZ%20Bildungszentrum%20Kassel%20GmbH!5e0!3m2!1sde!2sde!4v1624889223034!5m2!1sde!2sde" width="600" height="450" frameborder="0" style="border:0"></iframe>',
  };

  // Switch sources when clicked.
  function switchHotspot(id) {
    var wrapper = document.getElementById('iframespot');
    wrapper.innerHTML = hotspotHtml[id];
  }

  var switchElements = document.querySelectorAll('[data-source]');
  for (var i = 0; i < switchElements.length; i++) {
    var element = switchElements[i];
    addClickEvent(element);
  }

  function addClickEvent(element) {
    element.addEventListener('click', function () {
      switchHotspot(element.getAttribute('data-source'));
    });
  }

  /* ----------------------------------------------------- */
  /* --- MODAL-HOTSPOT --- */

  function createModalHotspotElement(hotspot) {

    var wrapper = document.createElement('div');

    if(hotspot.bgimg === "zdmi"){
      var icon = document.createElement('i');
      icon.classList.add('zmdi');
      icon.classList.add(hotspot.zdmi);
      icon.classList.add('zmdi-hc-3x');

      if(hotspot.color === "green"){
        icon.style.color = "var(--kolearn-green)";
      }

      else if(hotspot.color === "yellow"){
        icon.style.color = "var(--yellow)";
      }

      else if(hotspot.color === "red"){
        icon.style.color = "var(--red)";
      }

      else{
        icon.style.color = "var(--kolearn-blue)";
      }

    }

    else{
      var icon = document.createElement('img');
      icon.src = hotspot.bgimg;
      icon.classList.add('hotspot');
    }

    icon.addEventListener('click', function() {
      hotspotOpenModal(hotspot.modalnumber);
    });

    if(hotspot.teaser)
    {
      var divteaser = document.createElement('div');
      divteaser.style.textAlign = "center";
      divteaser.style.marginLeft = "-40px";

      var teaser = document.createElement('h3');
      teaser.classList.add('title');
      teaser.innerHTML = hotspot.teaser;
      divteaser.appendChild(teaser);
      wrapper.appendChild(divteaser);
    }

    wrapper.appendChild(icon);
    return wrapper;
  }

  /* --- ONLYTEXT-HOTSPOT --- */

  function createOnlyTextHotspotElement(hotspot){
    var divteaser = document.createElement('div');
    divteaser.id = "onlytext";
    divteaser.style.textAlign = "center";

    var teaser = document.createElement('h3');
    teaser.classList.add('title');
    teaser.innerHTML = hotspot.teaser;

    divteaser.appendChild(teaser);
    return divteaser;
  }

  /* --- EXPAND-HOTSPOT --- */
  function createExpandHotspotElement(hotspot) {

    /*
    var link = document.createElement('link');
    link.setAttribute("rel", "stylesheet");
    link.setAttribute("href", "css/hotspot-styles/expand.css");
    */

    var div = document.createElement('div');
    div.id = "expand";

    var h1 = document.createElement('h1');
    h1.classList.add('title');
    h1.innerHTML = hotspot.teaser;

    var img = document.createElement('img');
    img.classList.add('icon');
    img.src = hotspot.bgimg;

    var p = document.createElement('p');
    p.innerHTML = hotspot.text;

    div.appendChild(h1);
    div.appendChild(img);
    div.appendChild(p);
    return div;
  }


  /* --- HINTSPOT-HOTSPOT --- */
  function createHintspotHotspotElement(hotspot) {

    /*
    var link = document.createElement('link');
    link.setAttribute("rel", "stylesheet");
    link.setAttribute("href", "css/hotspot-styles/hintspot.css");
    */

    var div = document.createElement('div');
    div.id = "hintspot";
    div.classList.add('hint--right');
    div.classList.add('hint--info');
    div.classList.add('hint--bounce');
    div.setAttribute("data-hint", "hint.css!");

    var a = document.createElement('a');
    a.setAttribute("href", "#");
    a.setAttribute("target", "blank");

    var img = document.createElement('img');
    img.src = "img/hotspot-styles/hotspot.png";
    hotspot.bgimg = img.src;

    a.appendChild(img);
    div.appendChild(a);
    return div;
  }


  /* --- INFO-HOTSPOT --- */
  function createInfoCustomHotspotElement(hotspot) {

    /*
    var link = document.createElement('link');
    link.setAttribute("rel", "stylesheet");
    link.setAttribute("href", "css/hotspot-styles/info.css");
    */

    var div = document.createElement('div');
    div.id = "info";

    var div2 = document.createElement('div');
    div2.classList.add('icon_wrapper');

    var div3 = document.createElement('div');
    div3.classList.add('icon');

    var div4 = document.createElement('div');
    div4.id = "inner_icon";
    div4.classList.add('inner_icon');

    var div5 = document.createElement('div');
    div5.classList.add('icon1');

    var div6 = document.createElement('div');
    div6.classList.add('icon2');

    var div7 = document.createElement('div');
    div7.classList.add('tip');

    var p = document.createElement('p');
    p.innerHTML = hotspot.previewtext;

    var div8 = document.createElement('div');
    div8.classList.add('content');

    var div9 = document.createElement('div');
    div9.classList.add('image-wrapper');

    var img = document.createElement('img');
    img.src = hotspot.bgimg;

    var div10 = document.createElement('div');
    div10.classList.add('content-form');

    var p2 = document.createElement('p');
    p2.style.fontSize = "12px";
    p2.style.textAlign = "justify";
    p2.innerHTML = hotspot.text1;

    var p3 = document.createElement('p');
    p3.style.fontSize = "8px";
    p3.style.textAlign = "justify";
    p3.innerHTML = hotspot.text2;

    var div11 = document.createElement('div');

    var input = document.createElement('input');
    input.id = "PreNameInputText";
    input.setAttribute('type', 'text');
    input.setAttribute('placeholder', 'Vorname hier eingeben');

    var brtag1 = document.createElement('br');
    var brtag2 = document.createElement('br');

    var input2 = document.createElement('input');
    input2.id = "SurNameInputText";
    input2.setAttribute('type', 'text');
    input2.setAttribute('placeholder', 'Nachname hier eingeben');

    var brtag3 = document.createElement('br');
    var brtag4 = document.createElement('br');

    var input3 = document.createElement('input');
    input3.id = "EmailInputText";
    input3.setAttribute('type', 'text');
    input3.setAttribute('placeholder', 'E-Mail-Adresse hier eingeben');

    var brtag5 = document.createElement('br');
    var brtag6 = document.createElement('br');

    var p4 = document.createElement('p');
    p4.id = "result";
    p4.style.fontSize = "12px";
    p4.style.textAlign = "justify";

    var brtag7 = document.createElement('br');

    var divsubmitbutton = document.createElement('div');
    divsubmitbutton.style.textAlign = 'center';

    var submitbutton = document.createElement('button');
    submitbutton.id = "buttonemaildownload";
    submitbutton.classList.add('btn-submit');
    submitbutton.setAttribute('onclick', 'ValidateEmail()');
    submitbutton.setAttribute('role', 'button');
    submitbutton.innerHTML = 'Abschicken';

    var div12 = document.createElement('div');
    div12.classList.add('button-wrapper');

    var button = document.createElement('button');
    button.classList.add('close');
    button.innerHTML = 'X';

    div2.appendChild(div3);
    div3.appendChild(div4);
    div4.appendChild(div5);
    div4.appendChild(div6);

    div7.appendChild(p);

    div8.appendChild(div9);
    div8.appendChild(div10);
    div8.appendChild(div12);

    div9.appendChild(img);

    div10.appendChild(p2);
    div10.appendChild(div11);
    div10.appendChild(p3);

    div11.appendChild(input);
    div11.appendChild(brtag1);
    div11.appendChild(brtag2);
    div11.appendChild(input2);
    div11.appendChild(brtag3);
    div11.appendChild(brtag4);
    div11.appendChild(input3);
    div11.appendChild(brtag5);
    div11.appendChild(brtag6);
    div11.appendChild(p4);
    div11.appendChild(brtag7);
    div11.appendChild(divsubmitbutton);
    divsubmitbutton.appendChild(submitbutton);

    div12.appendChild(button);

    div.appendChild(div2);
    div.appendChild(div7);
    div.appendChild(div8);

    return div;
  }

  if(document.querySelector("#info .icon_wrapper")!== null){

    document.querySelector("#info .icon_wrapper").addEventListener('click', function() {
      document.querySelector("#info").classList.toggle('expanded');
      document.querySelector("#inner_icon").classList.toggle('closeIcon');
    });

    document.querySelector("#info .close").addEventListener('click', function() {
      document.querySelector("#info").classList.remove('expanded');
      document.querySelector("#inner_icon").classList.remove('closeIcon');
    });

  }

  /* --- REVEAL-HOTSPOT --- */
  function createRevealHotspotElement(hotspot) {

    /*
    var link = document.createElement('link');
    link.setAttribute("rel", "stylesheet");
    link.setAttribute("href", "css/hotspot-styles/reveal.css");
    */

    var div = document.createElement('div');
    div.id = "reveal";

    var img = document.createElement('img');
    img.src = "img/hotspot-styles/photo.png";

    var div2 = document.createElement('div');
    div2.classList.add('reveal-content');

    var img2 = document.createElement('img');
    img2.src = "img/hotspot-styles/photo.jpg";

    var p = document.createElement('p');
    p.innerHTML = "Place some text here";

    div2.appendChild(img2);
    div2.appendChild(p);
    div.appendChild(img);
    div.appendChild(div2);

    return div;
  }


  /* --- ROTATE-HOTSPOT --- */
  function createRotateHotspotElement(hotspot) {

    /*
    var link = document.createElement('link');
    link.setAttribute("rel", "stylesheet");
    link.setAttribute("href", "css/hotspot-styles/rotate.css");
    */

    var div = document.createElement('div');
    div.id = "rotate-hotspot";
    div.classList.add('rotate-hotspot');

    var div2 = document.createElement('div');
    div2.id = "rotateimgbgimg";
    div2.classList.add('rotate-img');

    var img = document.createElement('img');
    img.src = hotspot.bgimg;
    img.style.width = "100%";
    img.style.height = "100%";
    div2.appendChild(img);

    //div2.style.background = "white";
    //div2.style.background = "url('../../"+hotspot.bgimg+"')|no-repeat|center|center";
    //div2.style.background = "url('hotspot.bgimg')|no-repeat|center|center|!important";

    var div3 = document.createElement('div');
    div3.classList.add('rotate-content');

    var h1 = document.createElement('h1');
    h1.innerHTML = hotspot.header;

    var p = document.createElement('p');
    p.innerHTML = hotspot.text;

    div3.appendChild(h1);
    div3.appendChild(p);
    div.appendChild(div2);
    div.appendChild(div3);

    return div;
  }


  /* --- TEXTINFO-HOTSPOT --- */
  function createtextInfoHotspotElement(hotspot) {

    /*
    var link = document.createElement('link');
    link.setAttribute("rel", "stylesheet");
    link.setAttribute("href", "css/hotspot-styles/textInfo.css");
    */

    var div = document.createElement('div');
    div.id = "textInfo";

    var div2 = document.createElement('div');
    div2.classList.add('hotspot');

    var div3 = document.createElement('div');
    div3.classList.add('out');

    var div4 = document.createElement('div');
    div4.classList.add('in');

    var p = document.createElement('p');
    p.innerHTML = "Place some text here";

    var div5 = document.createElement('div');
    div5.classList.add('tooltip-content');

    div2.appendChild(div3);
    div2.appendChild(div4);
    div5.appendChild(p);
    div.appendChild(div2);
    div.appendChild(div5);

    return div;
  }


  /* --- TOOLTIP-HOTSPOT --- */
  function createtooltipHotspotElement(hotspot) {

    /*
    var link = document.createElement('link');
    link.setAttribute("rel", "stylesheet");
    link.setAttribute("href", "css/hotspot-styles/tooltip.css");
    */

    var div = document.createElement('div');
    div.id = "tooltip";

    var div2 = document.createElement('div');
    div2.classList.add('out');

    var div3 = document.createElement('div');
    div3.classList.add('in');

    var div4 = document.createElement('div');
    div4.classList.add('image');

    var div5 = document.createElement('tip');
    div5.classList.add('tip');

    var p = document.createElement('p');
    p.innerHTML = "Go to the Room";

    var img = document.createElement('img');
    img.src = "img/hotspot-styles/furnace.png";

    div2.appendChild(div3);
    div3.appendChild(div4);
    div5.appendChild(p);
    div5.appendChild(img);
    div.appendChild(div2);
    div.appendChild(div5);

    return div;
  }


  /* ----------------------------------------------------- */

  function createLinkHotspotElement(hotspot) {

    // Create wrapper element to hold icon and tooltip.
    var wrapper = document.createElement('div');
    wrapper.classList.add('hotspot');
    wrapper.classList.add('link-hotspot');

    // Create image element.
    var icon = document.createElement('img');
    icon.src = 'img/link.png';
    icon.classList.add('link-hotspot-icon');

    // Set rotation transform.
    var transformProperties = [ '-ms-transform', '-webkit-transform', 'transform' ];
    for (var i = 0; i < transformProperties.length; i++) {
      var property = transformProperties[i];
      icon.style[property] = 'rotate(' + hotspot.rotation + 'rad)';
    }

    // Add click event handler.
    /*wrapper.addEventListener('click', function() {
      switchScene(findSceneById(hotspot.target));
    });*/

    // Add click event handler - NEW VERSION WITH TARGET.
    wrapper.addEventListener('click', function() {
      if(hotspot.hasOwnProperty('new_scene_view_params'))
      {
        console.log('target view params detected, changing initialViewParameters');
        var lYaw   = hotspot.new_scene_view_params.yaw;
        var lPitch = hotspot.new_scene_view_params.pitch;
        findSceneById(hotspot.target).data.initialViewParameters.yaw = lYaw;
        findSceneById(hotspot.target).data.initialViewParameters.pitch = lPitch;
      }
      switchScene(findSceneById(hotspot.target));
    });

    // Prevent touch and scroll events from reaching the parent element.
    // This prevents the view control logic from interfering with the hotspot.
    stopTouchAndScrollEventPropagation(wrapper);

    // Create tooltip element.
    var tooltip = document.createElement('div');
    tooltip.classList.add('hotspot-tooltip');
    tooltip.classList.add('link-hotspot-tooltip');
    tooltip.innerHTML = findSceneDataById(hotspot.target).name;

    wrapper.appendChild(icon);
    wrapper.appendChild(tooltip);

    return wrapper;
  }

  function createInfoHotspotElement(hotspot) {

    // Create wrapper element to hold icon and tooltip.
    var wrapper = document.createElement('div');
    wrapper.classList.add('hotspot');
    wrapper.classList.add('info-hotspot');

    // Create hotspot/tooltip header.
    var header = document.createElement('div');
    header.classList.add('info-hotspot-header');

    // Create image element.
    var iconWrapper = document.createElement('div');
    iconWrapper.classList.add('info-hotspot-icon-wrapper');
    var icon = document.createElement('img');
    icon.src = 'img/info.png';
    icon.classList.add('info-hotspot-icon');
    iconWrapper.appendChild(icon);

    // Create title element.
    var titleWrapper = document.createElement('div');
    titleWrapper.classList.add('info-hotspot-title-wrapper');
    var title = document.createElement('div');
    title.classList.add('info-hotspot-title');
    title.innerHTML = hotspot.title;
    titleWrapper.appendChild(title);

    // Create close element.
    var closeWrapper = document.createElement('div');
    closeWrapper.classList.add('info-hotspot-close-wrapper');
    var closeIcon = document.createElement('img');
    closeIcon.src = 'img/close.png';
    closeIcon.classList.add('info-hotspot-close-icon');
    closeWrapper.appendChild(closeIcon);

    // Construct header element.
    header.appendChild(iconWrapper);
    header.appendChild(titleWrapper);
    header.appendChild(closeWrapper);

    // Create text element.
    var text = document.createElement('div');
    text.classList.add('info-hotspot-text');
    text.innerHTML = hotspot.text;

    // Place header and text into wrapper element.
    wrapper.appendChild(header);
    wrapper.appendChild(text);

    // Create a modal for the hotspot content to appear on mobile mode.
    var modal = document.createElement('div');
    modal.innerHTML = wrapper.innerHTML;
    modal.classList.add('info-hotspot-modal');
    document.body.appendChild(modal);

    var toggle = function() {
      wrapper.classList.toggle('visible');
      modal.classList.toggle('visible');
    };

    // Show content when hotspot is clicked.
    wrapper.querySelector('.info-hotspot-header').addEventListener('click', toggle);

    // Hide content when close icon is clicked.
    modal.querySelector('.info-hotspot-close-wrapper').addEventListener('click', toggle);

    // Prevent touch and scroll events from reaching the parent element.
    // This prevents the view control logic from interfering with the hotspot.
    stopTouchAndScrollEventPropagation(wrapper);

    return wrapper;
  }

  // Prevent touch and scroll events from reaching the parent element.
  function stopTouchAndScrollEventPropagation(element, eventList) {
    var eventList = [
      'touchstart', 'touchmove', 'touchend', 'touchcancel',
      'pointerdown', 'pointermove', 'pointerup', 'pointercancel',
      'wheel'
    ];
    for (var i = 0; i < eventList.length; i++) {
      element.addEventListener(eventList[i], function(event) {
        event.stopPropagation();
      });
    }
  }

  function findSceneById(id) {
    for (var i = 0; i < scenes.length; i++) {
      if (scenes[i].data.id === id) {
        return scenes[i];
      }
    }
    return null;
  }

  function findSceneDataById(id) {
    for (var i = 0; i < data.scenes.length; i++) {
      if (data.scenes[i].id === id) {
        return data.scenes[i];
      }
    }
    return null;
  }

  // Display the initial scene.
  //switchScene(scenes[0]);

  var sceneurl = window.location.hash.substr(1);
  if(sceneurl){
    var newscene = findSceneById(sceneurl);
    switchScene(newscene);
  }
  else{
    // Display the initial scene.
    switchScene(scenes[0]);
  }

})();
