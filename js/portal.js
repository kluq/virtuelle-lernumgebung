/* Own JS Code */

/*
var jahr = new Date().getFullYear()
document.getElementById('ausgabe').innerHTML = jahr;
*/

// Modal

// Get the modal
const modal = document.getElementsByClassName('modal');
const span = document.getElementsByClassName("close");

// Each Modal got an id of its index
let modalIdx = null;

// When the user clicks the button, open the modal
function openModal(idx) {
    // OLD
    //modalIdx = idx;
    //modal[idx].style.display = "block";

    const modalid = document.getElementById("myModal" + idx);
    modalid.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
function closeModal(idx) {
    // OLD
    //modal[idx].style.display = "none";

    const modalid = document.getElementById("myModal" + idx);
    modalid.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (e) {
    if (e.target === modal[modalIdx]) {
        modal[modalIdx].style.display = "none";
    }
}

// close the modal with hotkey [esc]
window.addEventListener("keydown", closeM);
function closeM(e) {
    if (e.keyCode === 27) {
        modal[modalIdx].style.display = "none";
    }
}

function getTextContent(modalIdx) {
    var textContent = document.getElementById(modalIdx).textContent;
    readText(textContent);
}

/* ------------------------------- */

// Open Modal Function

function hotspotOpenModal(args) {
    var idx = args;
    /*var panorama = document.getElementById("pano");
    var modalid = document.getElementById("myModal" + idx);
    panorama.appendChild(modalid);*/
    console.log(idx);
    openModal(idx);
}

/* ------------------------------- */

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();

function openTabs(evt, openTabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(openTabName).style.display = "block";
    evt.currentTarget.className += " active";
}

/* ------------------------------- */

// SLIDESHOW




/* ------------------------------- */