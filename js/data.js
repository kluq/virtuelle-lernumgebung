/*
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var data = {
  "scenes": [
    {
      "id": "0-projet-460",
      "name": "ProJet 460",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1440,
      "initialViewParameters": {
        "yaw": -0.24437791532025877,
        "pitch": 0.42364967682235743,
        "fov": 1.5575200506189628
      },
      "linkHotspots": [
        {
          "yaw": 2.3544741749935305,
          "pitch": 0.01975103372153697,
          "rotation": 0,
		  "text": "Nächste Szene"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.06882302399298453,
          "pitch": 0.03068759625077888,
          "title": "ProJet 460<br>",
          "text": "ProJet 460"
        },
        {
          "yaw": -0.79882302399298453,
          "pitch": 0.44068759625077888,
          "title": "Schritt-1-PC-starten-fuer-ProJet-460",
          "style": "modal",
          "bgimg": "zdmi",
          "zdmi": "zmdi-n-1-square",
          "color": "green",
          "modalnumber": "1",
        },
        {
          "yaw": -0.70882302399298453,
          "pitch": 0.10068759625077888,
          "title": "Schritt-2-3D-Print-starten",
          "style": "modal",
          "bgimg": "zdmi",
          "zdmi": "zmdi-n-2-square",
          "color": "green",
          "modalnumber": "2",
        },
        {
          "yaw": -0.06882302399298453,
          "pitch": 0.46068759625077888,
          "title": "Schritt-3-Ruesten-ProJet-460Plus-3D-Drucker",
          "style": "modal",
          "bgimg": "zdmi",
          "zdmi": "zmdi-n-3-square",
          "color": "green",
          "modalnumber": "3",
        },
        {
          "yaw": 0.07082302399298453,
          "pitch": -0.32068759625077888,
          "title": "Sicherheitshinweise",
          "style": "modal",
          "bgimg": "zdmi",
          "zdmi": "zmdi-alert-triangle",
          "color": "red",
          "modalnumber": "4",
        },
        {
          "yaw": 0.57082302399298453,
          "pitch": 0.18068759625077888,
          "title": "Hintergrundgeraeusche",
          "style": "modal",
          "bgimg": "zdmi",
          "zdmi": "zmdi-hearing",
          "color": "yellow",
          "modalnumber": "5",
        },
        {
          "yaw": 1.01082302399298453,
          "pitch": 0.18068759625077888,
          "title": "Newsletter-Modal-1",
          "number": "1",
          "style": "infocustom",
          "bgimg": "img/logos/KoLeArn_Logo.jpg",
          "previewtext": "Wenn Sie weitere Informationen zum Projekt KoLeArn möchten, dann klicken Sie hier!",
          "text1": "Wenn Sie an kostenlosen Vernetzungs-\naktivitäten oder Info-Veranstaltungen zur App Interesse haben, informieren wir Sie gerne. Bitte hinterlassen Sie dafür Ihre E-Mail-Adresse.",
          "text2": "*Ihr Name und Ihre E-Mail-Adresse wird nicht an Dritte weitergegeben. Zur Übermittlung wird Ihr Standard-Email-Programm verwendet.",
        }
	]
  },
 ],
  "name": "Sample Tour",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": true,
    "fullscreenButton": true,
    "viewControlButtons": true
  }
};
